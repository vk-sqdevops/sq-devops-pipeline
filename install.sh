#!/bin/bash

echo "Starting on $(date)..."

# Argument/Env Variable Verification
if [ \( -z "$AWS_ACCESS_KEY_ID" \) -a \( $# -ne 4 \) ]
  then
    echo -e "Invalid number of arguments/environment variables \n
		1) AWS_ACCESS_KEY_ID \n
		2) AWS_SECRET_ACCESS_KEY \n
		3) AWS_DEFAULT_REGION \n
		4) SETTINGS_FILE_PATH \n
	  Example: ./install.sh ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY} ${AWS_DEFATUL_REGION} ${SETTINGS_FILE_PATH} \n"
    exit
elif [ $# -eq 4 ]
	then
		export AWS_ACCESS_KEY_ID=$1
		export AWS_SECRET_ACCESS_KEY=$2
		export AWS_DEFAULT_REGION=$3
		export SETTINGS_FILE_PATH=$4
fi

# Getting AWS Account Number
echo "Getting AWS account number..." &&
AWS_ACCOUNT_NUMBER=$(aws iam get-user | jq '.User.Arn' | cut -d':' -f5) &&

# Defining s3 bucket name
ORGANIZATION_NAME=$(cat ${SETTINGS_FILE_PATH} | jq -r '.["organization"]') &&
PROJECT_NAME=$(cat ${SETTINGS_FILE_PATH} | jq -r '.["project"]') &&
ENVIRONMENT=$(cat ${SETTINGS_FILE_PATH} | jq -r '.["environment"]') &&
BUCKET_NAME="${AWS_ACCOUNT_NUMBER}-${ORGANIZATION_NAME}-${PROJECT_NAME}-${ENVIRONMENT}" &&

echo "Creating s3 bucket Name: ${BUCKET_NAME}" &&
aws s3api create-bucket --bucket $BUCKET_NAME --region $AWS_DEFAULT_REGION \
		--create-bucket-configuration LocationConstraint=$AWS_DEFAULT_REGION

echo "Uploading settings file to ${BUCKET_NAME}..." &&
aws s3 cp ${SETTINGS_FILE_PATH} s3://$BUCKET_NAME/settings.json &&

echo "Running infra setup..." &&
cd ./sq-devops-infra &&
./install.sh "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" "$AWS_DEFAULT_REGION" "$BUCKET_NAME" &&
cd ../

echo "Running sql setup..." &&
cd ./sq-devops-sql-server &&
./install.sh "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" "$AWS_DEFAULT_REGION" "$BUCKET_NAME" &&
cd ../

echo "Running api setup..." &&
cd ./sq-devops-api-server &&
./install.sh "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" "$AWS_DEFAULT_REGION" "$BUCKET_NAME" &&
cd ../

echo "Running web desktop setup..." &&
cd ./sq-devops-web-server-desktop &&
./install.sh "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" "$AWS_DEFAULT_REGION" "$BUCKET_NAME" &&
cd ../

echo "Finished on $(date)..."