# sq-devops-pipelilne

## Overview

* A coordinator repository that will link other devops repositories using git submodules
* The Bitbucket pipeline definitions will run its own scripts and will launch deploy scripts from other repositories
* The input into the pipeline is environment variables: aws key/secret/region & intitial settings file
* A script from this repository will create a new S3 bucket using a naming convention of acc#-org-project-env and upload the settings file into it
* A multi-step build process will call out to nested repos build scripts passing aws key/secret & path to S3 stored settings file
* Nested build processes will use the settings file and may update the settings file as well, e.g. sql server repo will inject connection string

## How to run

* When deploying to a new aws account remove terraform state files: `find . -name "terraform.tfstate*" -type f -delete`
* Git clone with `--recurse-submodules` flag to include submodules
* Cd into the new folder
* Run once per deployment host: `chmod +x ./*.sh && sudo ./setup.sh`
* Run for a new deployment or to update an existing deployment: 
```
export AWS_ACCESS_KEY_ID='your-key' &&
export AWS_SECRET_ACCESS_KEY='your-secret' &&
export AWS_DEFAULT_REGION='us-east-2' &&
export SETTINGS_FILE_PATH='./settings/settings-dev.json' &&
chmod +x ./*.sh && ./install.sh
```
* To pull recurseively: `git pull --recurse-submodules`

## How to remove

```
export AWS_ACCESS_KEY_ID='your-key' &&
export AWS_SECRET_ACCESS_KEY='your-secret' &&
export AWS_DEFAULT_REGION='us-east-2' &&chmod +x ./*.sh &&
export SETTINGS_FILE_PATH='./settings/settings-dev.json' &&
chmod +x ./*.sh && ./uninstall.sh
```

## To re-configure sub modules repositories:

* Update `./.gitmodules` file to point to nested repo urls
* Refresh from command line:
```
git submodule init 
git submodule update --init --recursive --remote
```
* More about submodules: https://chrisjean.com/git-submodules-adding-using-removing-and-updating/

## Ghost Instance Profile

* When tf execution fails you may end-up with a ghost instance profile not visible in aws web console
* Re-running terraform in such case will produce an error:
```
aws_iam_instance_profile.ec2-instance-profile: Error creating IAM instance profile sq-dev-ec2-instance-profile: 
  EntityAlreadyExists: Instance Profile sq-dev-ec2-instance-profile already exists.
  status code: 409, request id: 1b980b7a-f66c-11e8-9c0b-5194e40872a7
```
* A manual delete of an instance profile using [aws-cli](https://docs.aws.amazon.com/cli/latest/reference/iam/list-instance-profiles.html) will be required
* Run following command to list all instance profiles and to note ghost instance pro:
`aws iam list-instance-profiles`
* Then delete conflicting instance profile using its id e.g. :
`aws iam delete-instance-profile --instance-profile-name sq-dev-ec2-instance-profile`
