#!/bin/bash

# Argument/Env Variable Verification
if [ \( -z "$AWS_ACCESS_KEY_ID" \) -a \( $# -ne 4 \) ]
  then
    echo -e "Invalid number of arguments/environment variables \n
		1) AWS_ACCESS_KEY_ID \n
		2) AWS_SECRET_ACCESS_KEY \n
		3) AWS_DEFAULT_REGION \n
		4) SETTINGS_FILE_PATH \n
	  Example: ./install.sh ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY} ${AWS_DEFATUL_REGION} ${SETTINGS_FILE_PATH} \n"
    exit
elif [ $# -eq 4 ]
	then
		export AWS_ACCESS_KEY_ID=$1
		export AWS_SECRET_ACCESS_KEY=$2
		export AWS_DEFAULT_REGION=$3
		export BUCKET_NAME=$4
fi

# Get AWS Account Number
echo "Getting AWS account number..." &&
AWS_ACCOUNT_NUMBER=$(aws iam get-user | jq '.User.Arn' | cut -d':' -f5) &&

# Get Bucket Name
ORGANIZATION_NAME=$(cat ${SETTINGS_FILE_PATH} | jq -r '.["organization"]') &&
PROJECT_NAME=$(cat ${SETTINGS_FILE_PATH} | jq -r '.["project"]') &&
ENVIRONMENT=$(cat ${SETTINGS_FILE_PATH} | jq -r '.["environment"]') &&
BUCKET_NAME="${AWS_ACCOUNT_NUMBER}-${ORGANIZATION_NAME}-${PROJECT_NAME}-${ENVIRONMENT}" &&

echo 'Removing web server desktop...'
cd ./sq-devops-web-server-desktop &&
./uninstall.sh "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_KEY}" "${AWS_DEFAULT_REGION}" "${BUCKET_NAME}" &&
cd ../

echo 'Removing api server...'
cd ./sq-devops-api-server &&
./uninstall.sh "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_KEY}" "${AWS_DEFAULT_REGION}" "${BUCKET_NAME}" &&
cd ../

echo 'Removing sql server...'
cd ./sq-devops-sql-server &&
./uninstall.sh "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_KEY}" "${AWS_DEFAULT_REGION}" "${BUCKET_NAME}" &&
cd ../

echo 'Removing infra...'
cd ./sq-devops-infra &&
./uninstall.sh "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_KEY}" "${AWS_DEFAULT_REGION}" "${BUCKET_NAME}" &&
cd ../

# echo "Deleting S3 bucket: ${BUCKET_NAME}" &&
aws s3 rb s3://$BUCKET_NAME --force 
