# Download and install dependencies
echo 'Installing system dependencies...' &&
yum install -y epel-release
yum  -y groupinstall "Development Tools"
yum install git awscli zip wget jq npm -y &&
npm install -g tf-output &&

# Download and install dependencies
export terraform_version=0.11.10 &&
export terraform_install_dir="/opt/terraform" &&
wget -N  "https://releases.hashicorp.com/terraform/$terraform_version/terraform_${terraform_version}_linux_amd64.zip" &&
unzip -o terraform_${terraform_version}_linux_amd64.zip -d $terraform_install_dir &&
rm -f terraform_${terraform_version}_linux_amd64.zip &&

export PATH=$PATH:$terraform_install_dir

echo 'Verifying installation by initializing Terraform AWS provider...' &&
terraform init

echo 'Updating submodules...' &&
git submodule init
git submodule update --init --recursive --remote
